package it.polito.dp2.RNS.sol1;

import java.io.File;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import it.polito.dp2.RNS.ConnectionReader;
import it.polito.dp2.RNS.FactoryConfigurationError;
import it.polito.dp2.RNS.GateReader;
import it.polito.dp2.RNS.ParkingAreaReader;
import it.polito.dp2.RNS.RnsReader;
import it.polito.dp2.RNS.RnsReaderException;
import it.polito.dp2.RNS.RnsReaderFactory;
import it.polito.dp2.RNS.RoadSegmentReader;
import it.polito.dp2.RNS.VehicleReader;
import it.polito.dp2.RNS.sol1.jaxb.Connection;
import it.polito.dp2.RNS.sol1.jaxb.Gate;
import it.polito.dp2.RNS.sol1.jaxb.GateType;
import it.polito.dp2.RNS.sol1.jaxb.ObjectFactory;
import it.polito.dp2.RNS.sol1.jaxb.ParkingArea;
import it.polito.dp2.RNS.sol1.jaxb.Place;
import it.polito.dp2.RNS.sol1.jaxb.RoadNavigationSystem;
import it.polito.dp2.RNS.sol1.jaxb.RoadSegment;
import it.polito.dp2.RNS.sol1.jaxb.Vehicle;
import it.polito.dp2.RNS.sol1.jaxb.VehicleState;
import it.polito.dp2.RNS.sol1.jaxb.VehicleType;

public class RnsInfoSerializer {
	private RnsReader monitor;
	private ObjectFactory of;

	public static void main(String[] args) {
		final int INPUT_FILE_ERROR	= 1;
		final int SERIALIZER_ERROR	= 2;
		final int MARSHAL_ERROR		= 3;
		
		RnsInfoSerializer ris;
		RoadNavigationSystem rns = new RoadNavigationSystem();
		JAXBContext jc;
		Marshaller m;

		if(args.length != 1) {
			System.err.println("Please, insert an XML output file as a parameter");
			System.exit(INPUT_FILE_ERROR);
		}

		try {
			ris = new RnsInfoSerializer();

			Set<Place> places = ris.getGates();
			places.addAll(ris.getParkingAreas());
			places.addAll(ris.getRoadSegments());

			Set<Connection> connections = ris.getConnections();
			Set<Vehicle> vehicles = ris.getVehicles();

			rns.getPlace().addAll(places);
			rns.getConnection().addAll(connections);
			rns.getVehicle().addAll(vehicles);
		} catch(RnsReaderException e) {
			System.err.println("Could not instantiate data generator.");
			e.printStackTrace();
			System.exit(SERIALIZER_ERROR);
		}

		try {
			jc = JAXBContext.newInstance("it.polito.dp2.RNS.sol1.jaxb");
			m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(rns, new File(args[0]));
		} catch(JAXBException e) {
			System.err.println("Error during marshal operations.");
			e.printStackTrace();
			System.exit(MARSHAL_ERROR);
		}
	}

	public RnsInfoSerializer() throws RnsReaderException {
		RnsReaderFactory factory;
		try {
			factory = RnsReaderFactory.newInstance();
		} catch(FactoryConfigurationError fce) {
			fce.printStackTrace();
			throw new RnsReaderException(fce.getMessage());
		}
		
		monitor = factory.newRnsReader();
		of = new ObjectFactory();
	}

	public RnsInfoSerializer(RnsReader monitor) {
		this.monitor = monitor;
		of = new ObjectFactory();
	}

	public Set<Place> getGates() {
		Set<GateReader> reader = monitor.getGates(null);
		Set<Place> gates = new HashSet<>();

		for(GateReader gr : reader) {
			Place p = of.createPlace();
			Gate g = of.createGate();
			
			p.setId(gr.getId());
			p.setCapacity(BigInteger.valueOf(gr.getCapacity()));
			g.setType(GateType.valueOf(gr.getType().value()));
			
			p.setGate(g);
			gates.add(p);
		}

		return gates;
	}
	
	public Set<Place> getParkingAreas() {
		Set<ParkingAreaReader> reader = monitor.getParkingAreas(null);
		Set<Place> parkingAreas = new HashSet<>();

		for(ParkingAreaReader par : reader) {
			ParkingArea pa = of.createParkingArea();
			Place p = of.createPlace();
			
			p.setId(par.getId());
			p.setCapacity(BigInteger.valueOf(par.getCapacity()));

			for(String sr : par.getServices()) {
				ParkingArea.Service s = of.createParkingAreaService();
				s.setName(sr);
				pa.getService().add(s);
			}
			
			p.setParkingArea(pa);
			parkingAreas.add(p);
		}

		return parkingAreas;
	}
	
	public Set<Place> getRoadSegments() {
		Set<RoadSegmentReader> reader = monitor.getRoadSegments(null);
		Set<Place> roadSegments = new HashSet<>();
		
		for(RoadSegmentReader rr : reader) {
			RoadSegment rs = of.createRoadSegment();
			Place p = of.createPlace();
			
			p.setId(rr.getId());
			p.setCapacity(BigInteger.valueOf(rr.getCapacity()));
			rs.setName(rr.getName());
			rs.setRoadName(rr.getRoadName());
			
			p.setRoadSegment(rs);
			roadSegments.add(p);
		}

		return roadSegments;
	}
	
	public Set<Connection> getConnections() {
		Set<ConnectionReader> reader = monitor.getConnections();
		Set<Connection> connections = new HashSet<>();

		for(ConnectionReader cr : reader) {
			Connection c = of.createConnection();
			c.setFrom(cr.getFrom().getId());
			c.setTo(cr.getTo().getId());
			connections.add(c);
		}

		return connections;
	}

	public Set<Vehicle> getVehicles() {
		Set<VehicleReader> reader = monitor.getVehicles(null, null, null);
		Set<Vehicle> vehicles = new HashSet<>();

		for(VehicleReader vr : reader) {
			Vehicle v = of.createVehicle();
			v.setId(vr.getId());
			v.setState(VehicleState.valueOf(vr.getState().value()));
			v.setType(VehicleType.valueOf(vr.getType().toString()));
			v.setOrigin(vr.getOrigin().getId());
			v.setDestination(vr.getDestination().getId());
			v.setPosition(vr.getPosition().getId());
			try {
				v.setEntryTime(convertCalendar(vr.getEntryTime()));
			} catch (DatatypeConfigurationException e) {
				System.err.println("Error during creation of vehicle " + vr.getId());
			}
			vehicles.add(v);
		}

		return vehicles;
	}

	private XMLGregorianCalendar convertCalendar(Calendar calendar) throws DatatypeConfigurationException {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(calendar.getTime());
		XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		return xmlCalendar;
	}
}
