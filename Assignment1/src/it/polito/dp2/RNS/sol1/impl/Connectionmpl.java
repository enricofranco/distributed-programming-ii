package it.polito.dp2.RNS.sol1.impl;

import it.polito.dp2.RNS.ConnectionReader;
import it.polito.dp2.RNS.PlaceReader;

public class Connectionmpl implements ConnectionReader {
	private PlaceReader from;
	private PlaceReader to;
	
	public Connectionmpl(PlaceReader from, PlaceReader to) {
		this.from = from;
		this.to = to;
	}

	@Override
	public PlaceReader getFrom() {
		return from;
	}

	@Override
	public PlaceReader getTo() {
		return to;
	}

}
