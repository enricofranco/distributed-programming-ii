package it.polito.dp2.RNS.sol1.impl;

import java.util.Set;
import java.util.stream.Collectors;

import it.polito.dp2.RNS.ParkingAreaReader;
import it.polito.dp2.RNS.sol1.jaxb.Place;

public class ParkingAreaImpl extends PlaceImpl implements ParkingAreaReader {
	private Set<String> services;

	public ParkingAreaImpl(Place place) {
		super(place);
		services = place.getParkingArea().getService().stream()
				.map(s -> s.getName())
				.collect(Collectors.toSet());
	}
	
	@Override
	public Set<String> getServices() {
		return services;
	}

}
