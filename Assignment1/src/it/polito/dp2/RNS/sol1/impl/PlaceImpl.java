package it.polito.dp2.RNS.sol1.impl;

import java.util.HashSet;
import java.util.Set;

import it.polito.dp2.RNS.PlaceReader;
import it.polito.dp2.RNS.sol1.jaxb.Place;

public class PlaceImpl implements PlaceReader {
	private String id;
	private int capacity;
	private Set<PlaceReader> nextPlaces = new HashSet<>();
	
	public PlaceImpl(Place place) {
		id = place.getId();
		capacity = place.getCapacity().intValue();
	}
	
	@Override
	public int getCapacity() {
		return capacity;
	}

	@Override
	public Set<PlaceReader> getNextPlaces() {
		return nextPlaces;
	}

	@Override
	public String getId() {
		return id;
	}

}
