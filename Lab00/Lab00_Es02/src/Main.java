import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Main {

	final static int ROW_LENGTH = 6;
	static String filename;
	static GregorianCalendar mostRecentDate = new GregorianCalendar();
	
	public static void main(String[] args) {
		// Get the "it.polito.dp2.file" system property
		filename = System.getProperty("it.polito.dp2.file");
		mostRecentDate.clear();
		
		try(BufferedReader r = new BufferedReader(new FileReader(filename))) {
            r.lines()
            	.forEach(l -> {
            		String[] fields = l.split(" ", ROW_LENGTH);
		    		int hourOfDay = Integer.valueOf(fields[0]),
		    			minute = Integer.valueOf(fields[1]),
		    			second = Integer.valueOf(fields[2]),
		    			dayOfMonth = Integer.valueOf(fields[3]),
						month = Integer.valueOf(fields[4]) - 1,
						year = Integer.valueOf(fields[5]);
		        	GregorianCalendar g = new GregorianCalendar(year, month, dayOfMonth, hourOfDay, minute, second);
		        	if(g.compareTo(mostRecentDate) > 0) {
		        		mostRecentDate = g;
		        	}
            	});
		} catch(ArrayIndexOutOfBoundsException ex) {
            System.err.println("Unable to parse some line");                
		} catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + filename + "'");                
        } catch(IOException ex) {
            System.out.println("Error reading file '" + filename + "'");
        }
		
		System.out.println("Most recent date: " +
			mostRecentDate.get(GregorianCalendar.HOUR_OF_DAY) + ":" +
			mostRecentDate.get(GregorianCalendar.MINUTE) + ":" +
			mostRecentDate.get(GregorianCalendar.SECOND) + " " +
			mostRecentDate.get(GregorianCalendar.DAY_OF_MONTH) + " " +
			mostRecentDate.getDisplayName(GregorianCalendar.MONTH, GregorianCalendar.LONG, Locale.ENGLISH) + " " +
			mostRecentDate.get(GregorianCalendar.YEAR));
	}

}
