# Distributed Programming II

This repository contains the solution of the assignments related to the "Distributed programming II" course taught at Politecnico di Torino by professor Sisto Riccardo. Academic Year 2018-2019.