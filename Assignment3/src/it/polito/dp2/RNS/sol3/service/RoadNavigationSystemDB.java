package it.polito.dp2.RNS.sol3.service;

import java.math.BigInteger;
import java.net.URI;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.core.UriBuilder;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import it.polito.dp2.RNS.FactoryConfigurationError;
import it.polito.dp2.RNS.RnsReader;
import it.polito.dp2.RNS.RnsReaderException;
import it.polito.dp2.RNS.RnsReaderFactory;
import it.polito.dp2.RNS.lab2.BadStateException;
import it.polito.dp2.RNS.lab2.ModelException;
import it.polito.dp2.RNS.lab2.ServiceException;
import it.polito.dp2.RNS.lab2.UnknownIdException;
import it.polito.dp2.RNS.sol2.PathFinder;
import it.polito.dp2.RNS.sol3.service.exceptions.ConflictErrorException;
import it.polito.dp2.RNS.sol3.service.exceptions.GateNotFound;
import it.polito.dp2.RNS.sol3.service.exceptions.InvalidGateException;
import it.polito.dp2.RNS.sol3.service.exceptions.PathNotFound;
import it.polito.dp2.RNS.sol3.service.exceptions.PlaceNotFound;
import it.polito.dp2.RNS.sol3.service.exceptions.VehicleNotFound;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Connection;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.EnterRequest;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Gate;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.GateType;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.ParkingArea;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Path;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Place;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.PlaceRef;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.RoadSegment;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.UpdatePositionRequest;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.UpdateStateRequest;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Vehicle;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.VehicleState;

public class RoadNavigationSystemDB {
	private static RoadNavigationSystemDB rnsDB;
	
	private Map<String, Vehicle> vehicles;
	private Map<String, Place> places;
	private List<Connection> connections;
	private PathFinder pf;

	private RnsReader monitor;
	private UriBuilder resourceUri;
	
	private Logger logger;

	// Singleton pattern - Start
	private RoadNavigationSystemDB(UriBuilder uriBuilder) {
		// Attributes
		
		resourceUri = uriBuilder;
		logger = Logger.getLogger("db");
		
		places = new HashMap<>();
		vehicles = new ConcurrentHashMap<>();
		connections = new LinkedList<>();
		
		try {
			monitor = RnsReaderFactory.newInstance().newRnsReader();
		} catch (RnsReaderException e) {
			logger.log(Level.WARNING, "RnsReaderException. " + e.getMessage());
			throw new InternalServerErrorException();
		} catch (FactoryConfigurationError e) {
			logger.log(Level.WARNING, "FactoryConfigurationError. " + e.getMessage());
			throw new InternalServerErrorException();
		}
		
		pf = new PathFinder(monitor, getNeo4jURI().toString());
		
		// Loading
		loadInitialData();
		try {
			pf.reloadModel();
		} catch (ServiceException e) {
			logger.log(Level.WARNING, "[Database]\tNeo4j error " + e.getMessage());
			throw new InternalServerErrorException();
		} catch (ModelException e) {
			logger.log(Level.WARNING, "[Database]\tModel error " + e.getMessage());
			throw new InternalServerErrorException();
		}
	}
	
	public static RoadNavigationSystemDB getRoadNavigationSystemDB(URI resourceUri) {
		if(rnsDB == null) {
			rnsDB = new RoadNavigationSystemDB(UriBuilder.fromUri(resourceUri));
		}
		return rnsDB;
	}
	// Singleton pattern - End
	
	/**
	 * Reads data from the monitor, filling local collections
	 */
	private void loadInitialData() {
		monitor.getGates(null).stream().forEach(gr -> {
			Place p = new Place();
			Gate g = new Gate();
			
			p.setId(gr.getId());
			p.setCapacity(BigInteger.valueOf(gr.getCapacity()));
			g.setType(GateType.valueOf(gr.getType().value()));
			
			p.setGate(g);
			places.put(gr.getId(), p);
		});
		
		monitor.getParkingAreas(null).stream().forEach(par -> {
			ParkingArea pa = new ParkingArea();
			Place p = new Place();
			
			p.setId(par.getId());
			p.setCapacity(BigInteger.valueOf(par.getCapacity()));

			for(String sr : par.getServices()) {
				ParkingArea.Service s = new ParkingArea.Service();
				s.setName(sr);
				pa.getService().add(s);
			}
			
			p.setParkingArea(pa);
			places.put(par.getId(), p);
		});
		
		monitor.getRoadSegments(null).stream().forEach(rr -> {
			RoadSegment rs = new RoadSegment();
			Place p = new Place();
			
			p.setId(rr.getId());
			p.setCapacity(BigInteger.valueOf(rr.getCapacity()));
			rs.setName(rr.getName());
			rs.setRoadName(rr.getRoadName());
			
			p.setRoadSegment(rs);
			places.put(rr.getId(), p);
		});
		
		monitor.getConnections().stream().forEach(cr -> {
			Connection c = new Connection();
			
			String from = cr.getFrom().getId(),
					to = cr.getTo().getId();
			
			PlaceRef prf = generatePlaceRefFromId(from),
					prt = generatePlaceRefFromId(to);
			c.setFrom(prf);
			c.setTo(prt);
			connections.add(c);
		});
	}

	public synchronized Vehicle addVehicle(EnterRequest er) throws ConflictErrorException, PlaceNotFound, GateNotFound, InvalidGateException, PathNotFound {
		String msg = "Vehicle " + er.getId() + " trying to enter the system. "; // Header for logging

		if(vehicles.containsKey(er.getId())) {
			msg += "Vehicle " + er.getId() + " already existing.";
			logger.log(Level.INFO, msg);
			throw new ConflictErrorException(msg);
		}
		
		String origin = er.getOrigin(),
				destination = er.getDestination();
		if(! this.containsPlace(origin)) {
			msg += "Origin " + origin + " does not exists.";
			logger.log(Level.INFO, msg);
			throw new PlaceNotFound(msg);
		}
		
		if(! this.containsPlace(destination)) {
			msg += "Destination " + destination + " does not exists.";
			logger.log(Level.INFO, msg);
			throw new PlaceNotFound(msg);
		}
		
		Place p = getPlace(origin);
		if(p.getGate() == null) {
			msg += "Origin " + origin + " is not a Gate.";
			logger.log(Level.INFO, msg);
			throw new GateNotFound(msg);
		}

		Gate g = p.getGate();
		if(! g.getType().equals(GateType.IN) && ! g.getType().equals(GateType.INOUT)) {
			msg += "Origin " + origin + " is not a IN/INOUT Gate.";
			logger.log(Level.INFO, msg);
			throw new InvalidGateException(msg);
		}
			
		List<String> path = requestPath(origin, destination);
		if(path == null) {
			msg += "Path cannot be found between " + origin + " and " + destination + ".";
			logger.log(Level.INFO, msg);
			throw new PathNotFound(msg);
		}
		
		logger.log(Level.INFO, msg + "Setting properties.");
		Vehicle vehicle = new Vehicle();
		try {
			vehicle.setId(er.getId());
			
			// Add place references
			vehicle.setOrigin(generatePlaceRefFromId(origin));
			vehicle.setPosition(generatePlaceRefFromId(origin));
			vehicle.setDestination(generatePlaceRefFromId(destination));
			
			vehicle.setPath(generatePathFromList(path));
			vehicle.setState(VehicleState.IN_TRANSIT);
			vehicle.setType(er.getType());

			vehicle.setEntryTime(convertCalendar(new Date())); // May generate exceptions
		} catch (DatatypeConfigurationException e) {
			msg += "Not created.";
			logger.log(Level.INFO, msg);
			throw new InternalServerErrorException(msg);
		}
		
		vehicles.put(vehicle.getId(), vehicle);
		return vehicle;
	}
	
	/**
	 * Interacts with Neo4j retrieving a path
	 * @param origin origin place id
	 * @param destination destination place id
	 * @return List of String containing the ids of the traversed places
	 * @throws InternalServerErrorException if some error occurred with Neo4j interaction
	 */
	private List<String> requestPath(String origin, String destination) throws InternalServerErrorException {
		try {
			return pf.findShortestPaths(origin, destination, Integer.MAX_VALUE).stream()
				.findFirst()
				.orElse(null);
		} catch (UnknownIdException | BadStateException | ServiceException e) {
			String msg = "[Database]\tNeo4j Error retrieving path " + origin + " -> " + destination;
			logger.log(Level.WARNING, msg + e.getMessage());
			throw new InternalServerErrorException();
		}
	}
	
	/**
	 * Removes a vehicle from the system, if possible
	 * @param id
	 * @param outGate
	 * @return
	 * @throws VehicleNotFound if {@code id} does not represent a valid {@code Vehicle}
	 * @throws PlaceNotFound if {@code outGate} does not represent a valid {@code Place}
	 * @throws GateNotFound if {@code outGate} does not represent a valid {@code Gate}
	 * @throws InvalidGateException if {@code outGate} does not represent a valid {@code INOUT} or {@code Out} {@code Gate}
	 * @throws PathNotFound if {@code outGate} is not reachable from current position
	 * @throws ServiceException 
	 * @throws BadStateException 
	 * @throws UnknownIdException 
	 */
	public synchronized Vehicle exitVehicle(String id, String outGate) throws VehicleNotFound, PlaceNotFound, GateNotFound, InvalidGateException, PathNotFound {
		String msg = "Vehicle " + id + " trying to exit from gate " + outGate + ". "; // Header for logging
		
		if(! vehicles.containsKey(id)) {
			msg += "Vehicle " + id + " not found.";
			logger.log(Level.INFO, msg);
			throw new VehicleNotFound(msg);
		}
		
		if(! containsPlace(outGate)) {
			msg += "Place " + outGate + " does not exist.";
			logger.log(Level.INFO, msg);
			throw new PlaceNotFound(msg);
		}
		
		Place p = places.get(outGate);
		Gate g = p.getGate();
		if(p.getGate() == null) {
			msg += "Gate " + outGate + " is not a Gate.";
			logger.log(Level.INFO, msg);
			throw new GateNotFound(msg);
		}
		
		if(! g.getType().equals(GateType.OUT) && ! g.getType().equals(GateType.INOUT)) {
			msg += "Gate " + outGate + " is not a INOUT/OUT Gate.";
			logger.log(Level.INFO, msg);
			throw new InvalidGateException(msg);
		}	
		
		Vehicle vehicle = vehicles.get(id);
		String currentPosition = vehicle.getPosition().getId();
		List<String> pathToGate = requestPath(currentPosition, outGate);
		
		if(pathToGate == null) {
			msg += "Path cannot be found between " + vehicle.getPosition().getId() + " and " + outGate + ".";
			logger.log(Level.INFO, msg);
			throw new PathNotFound(msg);
		}
		
		vehicles.remove(id);
		return vehicle;
	}
	
	public synchronized Vehicle updateVehiclePosition(String id, UpdatePositionRequest ur) throws PlaceNotFound, PathNotFound {
		String msg = "Vehicle " + id + " requesting position " + ur.getPosition() + ". "; // Header for logging

		if(! vehicles.containsKey(id)) {
			msg += "Vehicle " + id + " not found.";
			logger.log(Level.INFO, msg);
			throw new VehicleNotFound(msg);
		}
		
		Vehicle vehicle = vehicles.get(id);
		String newPosition = ur.getPosition(),
				currentPosition = vehicle.getPosition().getId();
		// Check if position is different
		if(newPosition != null && ! currentPosition.equalsIgnoreCase(newPosition)) {
			updatePosition(vehicle, newPosition, msg);
			return vehicle;	
		} else {
			return null;	// No modification
		}
	}	
	
	public synchronized Vehicle updateVehicleState(String id, UpdateStateRequest ur) {
		String msg = "Vehicle " + id + " requesting state " + ur.getState() + ". "; // Header for logging

		if(! vehicles.containsKey(id)) {
			msg += "Vehicle " + id + " not found.";
			logger.log(Level.INFO, msg);
			throw new VehicleNotFound(msg);
		}
		
		Vehicle vehicle = vehicles.get(id);
		VehicleState newState = ur.getState(),
				currentState = vehicle.getState();
		// Check if state is different
		if(newState != null && ! currentState.equals(newState)) {
			updateState(vehicle, newState);
			return vehicle;	
		} else {
			return null;	// No modification
		}
	}
		
	private void updatePosition(Vehicle vehicle, String newPlace, String header) throws PlaceNotFound, PathNotFound {
		if(!containsPlace(newPlace))
			throw new PlaceNotFound(header + "Place " + newPlace + " not found");
		
		// Prepare position, in case request is correct
		PlaceRef position = new PlaceRef();
		position.setId(newPlace);
		position.setURI(generatePlaceUri(newPlace).toString());
		
		if(vehicleTraversePlace(vehicle, newPlace)) {
			// Place on the current path, update position
			vehicle.setPosition(position);
			logger.log(Level.INFO, header + 
					"Vehicle " + vehicle.getId()
					+ " Already traverses place " + newPlace 
					+ " (Position updated)");
			return;
		}
		
		// Place is not on the current path
		List<String> path = requestPath(vehicle.getPosition().getId(), newPlace);
		if(path == null) { // Place is not reachable from the previous position
			logger.log(Level.INFO, header + 
					"Vehicle " + vehicle.getId()
					+ " Place " + newPlace + " not reachable from " + vehicle.getPosition().getId());
			throw new PathNotFound("Place " + newPlace + " not reachable from " + vehicle.getPosition().getId());
		}
		
		// Place is reachable from the previous position
		vehicle.setPosition(position);
		
		path = requestPath(newPlace, vehicle.getDestination().getId());
		if(path != null) {
			// Destination is reachable from the new place
			vehicle.setPath(generatePathFromList(path));
			logger.log(Level. INFO, header + 
					"Vehicle " + vehicle.getId()
					+ " Destination " + vehicle.getDestination().getId() + " reachable from " + newPlace);
			return;
		}
		
		// Destination is not reachable from the new position
		logger.log(Level.INFO, header + 
				"Vehicle " + vehicle.getId()
				+ " Destination " + vehicle.getDestination().getId() + " not reachable from " + newPlace);
		vehicle.setPath(new Path());
	}
	
	private void updateState(Vehicle vehicle, VehicleState newState) {
		vehicle.setState(newState);
	}
	
	// Getters
	
	public Collection<Place> getPlaces() {
		return places.values();
	}
		
	public Place getPlace(String id) {
		return places.get(id);
	}
		
	public Collection<Connection> getConnections() {
		return connections;
	}
		
	public Collection<Place> getGates() {
		return places.values().stream()
				.filter(p -> p.getGate() != null)
				.collect(Collectors.toSet());
	}
		
	public Collection<Place> getParkingAreas() {
		return places.values().stream()
			.filter(p -> p.getParkingArea() != null)
			.collect(Collectors.toSet());
	}
		
	public Collection<Place> getRoadSegments() {
		return places.values().stream()
				.filter(p -> p.getRoadSegment() != null)
				.collect(Collectors.toSet());
	}
		
	public Collection<Vehicle> getVehicles(String placeId) {
		return vehicles.values().stream()
				.filter(v -> (placeId != null ? v.getPosition().getId().equals(placeId) : true))		
				.collect(Collectors.toSet());
	}
		
	public Vehicle getVehicle(String id) {
		return vehicles.get(id);
	}
	
	// Utilities
	
	private URI getNeo4jURI() {
		String baseURI = System.getProperty("it.polito.dp2.RNS.lab3.Neo4JURL");
		if(baseURI == null) // Property not set, use default value
			baseURI = "http://localhost:7474/db";
		
		return UriBuilder.fromUri(baseURI).build();
	}
	
	private boolean containsPlace(String id) {
		return (this.getPlace(id) != null);
	}
	
	private PlaceRef generatePlaceRefFromId(String id) {
		PlaceRef pr = new PlaceRef();
		pr.setId(id);
		pr.setURI(resourceUri.clone().path("places").path(id).toTemplate());
		return pr;
	}

	private URI generatePlaceUri(String id) {
		return resourceUri.clone().path("places").path(id).build();
	}
	
	private boolean vehicleTraversePlace(Vehicle vehicle, String placeId) {
		return vehicle.getPath().getPlace().stream()
				.map(PlaceRef::getId)
				.collect(Collectors.toList())
				.contains(placeId);
	}
	
	/**
	 * @param path List<String> containing the id of the places
	 * @return a Path object to be stored in a Vehicle
	 */
	private Path generatePathFromList(List<String> path) {
		Path vehiclePath = new Path();
		for(String placeId : path) {
			PlaceRef pr = new PlaceRef();
			pr.setId(placeId);
			pr.setURI(generatePlaceUri(placeId).toString());
			vehiclePath.getPlace().add(pr);
		}
		return vehiclePath;
	}

	private XMLGregorianCalendar convertCalendar(Date date) throws DatatypeConfigurationException {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(date);
		XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		return xmlCalendar;
	}
	
}
