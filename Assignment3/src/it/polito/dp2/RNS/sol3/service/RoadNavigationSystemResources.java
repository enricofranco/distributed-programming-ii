package it.polito.dp2.RNS.sol3.service;

import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.polito.dp2.RNS.sol3.service.exceptions.ExceptionCodes;
import it.polito.dp2.RNS.sol3.service.exceptions.VehicleNotFound;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Connections;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.EnterRequest;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Place;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Places;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.RnsSystem;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.UpdatePositionRequest;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.UpdateStateRequest;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Vehicle;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Vehicles;

@Path("/webapi")
@Api(value = "/webapi")
public class RoadNavigationSystemResources {
	private UriInfo uriInfo;
	private RoadNavigationSystemService service;
	private URI baseURI;
	
	private Logger logger = Logger.getLogger("resource");

	public RoadNavigationSystemResources(@Context UriInfo uriInfo) {
		this.uriInfo = uriInfo;
		this.baseURI = getBaseURI();
		this.service = new RoadNavigationSystemService(baseURI);
	}
	
	@GET
	@ApiOperation(
			value = "Reads the main resource",
			notes = "Returns URIs to navigate the system"
	)
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "Ok")
			})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public RnsSystem getRnsSystem() {
		RnsSystem rns = new RnsSystem();
		UriBuilder root = uriInfo.getAbsolutePathBuilder();
		rns.setSelf(root.toTemplate());
		
		UriBuilder vehicles = root.clone().path("vehicles"),
				places = root.clone().path("places"),
				connections = root.clone().path("connections"),
				gates = places.clone().path("gates"),
				parkingAreas = places.clone().path("parkingAreas"),
				roadSegments = places.clone().path("roadSegments");
		rns.setPlaces(places.toTemplate());
		rns.setGates(gates.toString());
		rns.setParkingareas(parkingAreas.toString());
		rns.setRoadsegments(roadSegments.toString());
		rns.setVehicles(vehicles.toTemplate());
		rns.setConnections(connections.toTemplate());
		return rns;
	}
	
	@GET
	@Path("/places")
	@ApiOperation(
			value = "Reads the place resource",
			notes = "Returns URIs to navigate the places")
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "Ok"),
					@ApiResponse(code = 403, message = "Forbidden")
			})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Places getPlaces(
			@HeaderParam("admin") @ApiParam("Defines if the client is an administrator or not") boolean isAdmin) {
		if(!isAdmin)
			throw new ForbiddenException();
		return service.getPlaces();
	}
	
	@GET
    @Path("/places/{id}")
    @ApiOperation(
    		value = "Finds place by ID",
    		notes = "Returns a single place"
	)
    @ApiResponses(
    		value = {
    				@ApiResponse(code = 200, message = "Ok"),
    				@ApiResponse(code = 404, message = "Not Found")
    		}
    )
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
    public Place getPlace(
    		@PathParam("id") @ApiParam("The ID of the place to return") String id) {
		Place p = service.getPlace(id);
		if(p == null)
			throw new NotFoundException();
		return p;
    }
	
	@GET
	@Path("/places/gates")
	@ApiOperation(
			value = "Reads the gate resources",
			notes = "Returns a collection of gates")
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "Ok"),
					@ApiResponse(code = 403, message = "Forbidden")
			})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Places getGates(
			@HeaderParam("admin") @ApiParam("Defines if the client is an administrator or not") boolean isAdmin) {
		if(!isAdmin)
			throw new ForbiddenException();
		return service.getGates();
	}
	
	@GET
	@Path("/places/parkingareas")
	@ApiOperation(
			value = "Reads the parking area resources",
			notes = "Returns a collection of gates")
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "Ok"),
					@ApiResponse(code = 403, message = "Forbidden")
			})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Places getParkingAreas(
			@HeaderParam("admin") @ApiParam("Defines if the client is an administrator or not") boolean isAdmin) {
		if(!isAdmin)
			throw new ForbiddenException();
		return service.getParkingAreas();
	}
	
	@GET
	@Path("/places/roadsegments")
	@ApiOperation(
			value = "Reads the road segment resources",
			notes = "Returns a collection of road segments")
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "Ok"),
					@ApiResponse(code = 403, message = "Forbidden")
			})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Places getRoadSegments(
			@HeaderParam("admin") @ApiParam("Defines if the client is an administrator or not") boolean isAdmin) {
		if(!isAdmin)
			throw new ForbiddenException();
		return service.getRoadSegments();
	}
	
	@GET
	@Path("/connections")
	@ApiOperation(
			value = "Reads the connection resources",
			notes = "Returns a collection of connections")
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "Ok"),
					@ApiResponse(code = 403, message = "Forbidden")
			})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Connections getConnections(
			@HeaderParam("admin") @ApiParam("Defines if the client is an administrator or not") boolean isAdmin) {
		if(!isAdmin)
			throw new ForbiddenException();
		return service.getConnections(null, null);
	}
	
	@GET
	@Path("/vehicles")
	@ApiOperation(
			value = "Reads the vehicle resources",
			notes = "Returns a collection of vehicles")
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "Ok"),
					@ApiResponse(code = 403, message = "Forbidden")
			})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Vehicles getVehiclesInPlace(
			@HeaderParam("admin") @ApiParam("Defines if the client is an administrator or not") boolean isAdmin,
			@QueryParam("placeId") @ApiParam("The ID of the vehicle to return") String placeId) {
		if(!isAdmin)
			throw new ForbiddenException();
		return service.getVehicles(placeId);
	}
	
	@GET
    @Path("/vehicles/{id}")
    @ApiOperation(
    		value = "Finds vehicle by ID",
    		notes = "Returns a single vehicle"
	)
    @ApiResponses(
    		value = {
    				@ApiResponse(code = 200, message = "Ok"),
    				@ApiResponse(code = 404, message = "Not Found"),
					@ApiResponse(code = 403, message = "Forbidden")
    		}
    )
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
    public Vehicle getVehicle(
    		@HeaderParam("admin") @ApiParam("Defines if the client is an administrator or not") boolean isAdmin,
    		@PathParam("id") @ApiParam("The ID of the vehicle to return") String id) {
		if(!isAdmin)
			throw new ForbiddenException();
		
		Vehicle vehicle = service.getVehicle(id);
		if(vehicle == null)
			throw new NotFoundException();
		
		return vehicle;
    }
	
	@POST
	@Path("/vehicles")
    @ApiOperation(
    		value = "Adds a vehicle to the system"
	)
    @ApiResponses(
    		value = {
    				@ApiResponse(code = 201, message = "Created"),
    				@ApiResponse(code = 400, message = "Bad Request"),
    				@ApiResponse(code = 409, message = "Conflict"),
    				@ApiResponse(code = ExceptionCodes.GATE_NOT_FOUND, message = "Gate not found"),
    				@ApiResponse(code = ExceptionCodes.INVALID_GATE, message = "Invalid gate"),
    				@ApiResponse(code = ExceptionCodes.PATH_NOT_FOUND, message = "Path not found")
    		}
    )
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Response postVehicle(
			@ApiParam("Vehicle object that needs to be added to the system") EnterRequest er) {
		logger.log(Level.INFO, "Received POST request for vehicle " + er.getId());
		String vehicleId = er.getId();
		UriBuilder builder = uriInfo.getAbsolutePathBuilder().path(vehicleId);
		URI self = builder.build();
		
		// Vehicle properties
		Vehicle vehicle = service.addVehicle(er);
		vehicle.setSelf(self.toString());

		// Access granted
		return Response.created(self).entity(vehicle).build();
	}
	
	@PUT
	@Path("/vehicles/{id}/position")
    @ApiOperation("Update the position of an existing vehicle")
    @ApiResponses(
    		value = {
    				@ApiResponse(code = 200, message = "Ok"),
    				@ApiResponse(code = 204, message = "No content"),
    				@ApiResponse(code = 400, message = "Bad Request"),
    				@ApiResponse(code = 404, message = "Not found"),
    				@ApiResponse(code = ExceptionCodes.PLACE_NOT_FOUND, message = "Place not found"),
    				@ApiResponse(code = ExceptionCodes.VEHICLE_NOT_FOUND, message = "Vehicle not found"),
    				@ApiResponse(code = ExceptionCodes.PATH_NOT_FOUND, message = "Path not found")
    		}
    )
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Response updateVehiclePosition(
			@PathParam("id") @ApiParam("The ID of the vehicle to return") String id,
			@ApiParam("Update position request") UpdatePositionRequest ur) {
		logger.log(Level.INFO, "Received PUT request for changing position of vehicle " + id);
		
		Vehicle vehicle = service.updateVehiclePosition(id, ur);
		if(vehicle != null)
			return Response.ok().entity(vehicle).build();
		else
			return Response.noContent().build();
	}
	
	@PUT
	@Path("/vehicles/{id}/state")
    @ApiOperation("Update the state of an existing vehicle")
    @ApiResponses(
    		value = {
    				@ApiResponse(code = 200, message = "Ok"),
    				@ApiResponse(code = 204, message = "No content"),
    				@ApiResponse(code = 400, message = "Bad Request"),
    				@ApiResponse(code = 404, message = "Not found"),
    				@ApiResponse(code = ExceptionCodes.PLACE_NOT_FOUND, message = "Place not found"),
    				@ApiResponse(code = ExceptionCodes.VEHICLE_NOT_FOUND, message = "Vehicle not found"),
    				@ApiResponse(code = ExceptionCodes.PATH_NOT_FOUND, message = "Path not found")
    		}
    )
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Response updateVehicleState(
			@PathParam("id") @ApiParam("The ID of the vehicle to return") String id,
			@ApiParam("Update state request") UpdateStateRequest ur) {
		logger.log(Level.INFO, "Received PUT request for changing state of vehicle " + id);
		
		Vehicle vehicle = service.updateVehicleState(id, ur);
		if(vehicle != null)
			return Response.ok().entity(vehicle).build();
		else
			return Response.noContent().build();
	}
	
	@DELETE
	@Path("vehicles/{id}")
	@ApiOperation(
			value = "Deletes an existing vehicle",
			notes = "Delete a vehicle given its ID"
	)
	@ApiResponses(
			value = {
					@ApiResponse(code = 204, message = "No content"),
					@ApiResponse(code = 400, message = "Bad request"),
					@ApiResponse(code = ExceptionCodes.INVALID_GATE, message = "Invalid gate"),
					@ApiResponse(code = ExceptionCodes.PATH_NOT_FOUND, message = "Path not found"),
					@ApiResponse(code = ExceptionCodes.GATE_NOT_FOUND, message = "Gate not found"),
					@ApiResponse(code = ExceptionCodes.VEHICLE_NOT_FOUND, message = "Vehicle not found")
			}
	)
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public void deleteVehicle(
			@ApiParam(value = "Defines if the client is an administrator or not") @HeaderParam("admin") boolean isAdmin,
			@ApiParam("The ID of the vehicle to remove") @PathParam("id") String id,
			@ApiParam(value = "The ID of the gate from which to exit") @QueryParam("gate") @DefaultValue("") String outGate) {
		logger.log(Level.INFO, "Received exit request for vehicle " + id + " from gate " + outGate);
		
		if(outGate.equals(""))
			throw new BadRequestException("outGate not set");
		
		Vehicle vehicle = service.deleteVehicle(id, outGate, isAdmin);
		
		if(vehicle == null)
			throw new VehicleNotFound("Vehicle " + id + " not found");
	}
	
	private URI getBaseURI() {
		String baseURI = System.getProperty("it.polito.dp2.RNS.lab3.URL");
		if(baseURI == null) // Property not set, use default value
			baseURI = "http://localhost:8080/RnsSystem/rest";
		
		return UriBuilder.fromUri(baseURI)
				.build();
	}
	
}
