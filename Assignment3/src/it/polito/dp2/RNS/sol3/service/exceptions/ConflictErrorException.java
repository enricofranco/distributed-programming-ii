package it.polito.dp2.RNS.sol3.service.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class ConflictErrorException extends ClientErrorException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1198542400629453838L;
	private static final int CODE = ExceptionCodes.CONFLICT;

	public ConflictErrorException() {
		super(CODE);
	}
	
	public ConflictErrorException(String message) {
		super(Response.status(CODE).entity(message).build());
	}

}
