package it.polito.dp2.RNS.sol3.service;

import java.math.BigInteger;
import java.net.URI;
import java.util.stream.Collectors;

import it.polito.dp2.RNS.sol3.service.exceptions.ConflictErrorException;
import it.polito.dp2.RNS.sol3.service.exceptions.GateNotFound;
import it.polito.dp2.RNS.sol3.service.exceptions.InvalidGateException;
import it.polito.dp2.RNS.sol3.service.exceptions.PathNotFound;
import it.polito.dp2.RNS.sol3.service.exceptions.PlaceNotFound;
import it.polito.dp2.RNS.sol3.service.exceptions.VehicleNotFound;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Connections;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.EnterRequest;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Place;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Places;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.UpdatePositionRequest;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.UpdateStateRequest;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Vehicle;
import it.polito.dp2.RNS.sol3.service.jaxb.rnsSystem.Vehicles;

public class RoadNavigationSystemService {
	private RoadNavigationSystemDB db;
	
	public RoadNavigationSystemService(URI baseURI) {
		db = RoadNavigationSystemDB.getRoadNavigationSystemDB(baseURI);
	}
	
	public Places getPlaces() {
		Places gates = new Places();
		gates.getPlace().addAll(db.getPlaces());
		gates.setTotalPages(BigInteger.valueOf(1));
		gates.setPage(BigInteger.valueOf(0));
		return gates;
	}
	
	public Places getGates() {
		Places gates = new Places();
		gates.getPlace().addAll(db.getGates());
		gates.setTotalPages(BigInteger.valueOf(1));
		gates.setPage(BigInteger.valueOf(0));
		return gates;
	}
	
	public Places getParkingAreas() {
		Places parkingAreas = new Places();
		parkingAreas.getPlace().addAll(db.getParkingAreas());
		parkingAreas.setTotalPages(BigInteger.valueOf(1));
		parkingAreas.setPage(BigInteger.valueOf(0));
		return parkingAreas;
	}
	
	public Places getRoadSegments() {
		Places roadSegments = new Places();
		roadSegments.getPlace().addAll(db.getRoadSegments());
		roadSegments.setTotalPages(BigInteger.valueOf(1));
		roadSegments.setPage(BigInteger.valueOf(0));
		return roadSegments;
	}
	
	public Place getPlace(String id) {
		return db.getPlace(id);
	}
	
	public Connections getConnections(String from, String to) {
		Connections connections = new Connections();
		connections.getConnection().addAll(
				db.getConnections().stream()
				.filter(c -> (from != null ? c.getFrom().getId().equals(from) : true))
				.filter(c -> (to != null ? c.getTo().getId().equals(to) : true))
				.collect(Collectors.toList()));
		connections.setTotalPages(BigInteger.valueOf(1));
		connections.setPage(BigInteger.valueOf(0));
		return connections;
	}
	
	public Vehicles getVehicles(String placeId) {
		Vehicles vehicles = new Vehicles();
		vehicles.getVehicle().addAll(db.getVehicles(placeId));
		vehicles.setTotalPages(BigInteger.valueOf(1));
		vehicles.setPage(BigInteger.valueOf(0));
		return vehicles;
	}
	
	public Vehicle getVehicle(String id) {
		return db.getVehicle(id);
	}
	
	public Vehicle deleteVehicle(String id, String outGate, boolean admin) throws VehicleNotFound, PlaceNotFound, GateNotFound, InvalidGateException, PathNotFound {
//		TODO A request from an administrator must be always accepted and should call another method, not implemented here
		return db.exitVehicle(id, outGate);
	}
	
	public Vehicle addVehicle(EnterRequest er) throws ConflictErrorException, PlaceNotFound, GateNotFound, InvalidGateException, PathNotFound {
		return db.addVehicle(er);
	}
	
	public Vehicle updateVehiclePosition(String id, UpdatePositionRequest ur) throws VehicleNotFound, PlaceNotFound, PathNotFound {
		return db.updateVehiclePosition(id, ur);
	}
	
	public Vehicle updateVehicleState(String id, UpdateStateRequest ur) throws VehicleNotFound {
		return db.updateVehicleState(id, ur);
	}
	
}
