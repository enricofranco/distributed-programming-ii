package it.polito.dp2.RNS.sol3.vehClient;

import it.polito.dp2.RNS.lab3.VehClient;
import it.polito.dp2.RNS.lab3.VehClientException;

public class VehClientFactory extends it.polito.dp2.RNS.lab3.VehClientFactory {

	@Override
	public VehClient newVehClient() throws VehClientException {
		try {
			return new VehClientImpl();
		} catch(Exception e) {
			throw new VehClientException("VehClient implementation is not available or cannot be instantiated.");
		}
	}

}
