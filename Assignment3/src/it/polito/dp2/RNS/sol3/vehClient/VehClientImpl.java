package it.polito.dp2.RNS.sol3.vehClient;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import it.polito.dp2.RNS.lab3.EntranceRefusedException;
import it.polito.dp2.RNS.lab3.ServiceException;
import it.polito.dp2.RNS.lab3.UnknownPlaceException;
import it.polito.dp2.RNS.lab3.VehClient;
import it.polito.dp2.RNS.lab3.WrongPlaceException;
import it.polito.dp2.RNS.sol3.vehClient.jaxb.rnsSystem.EnterRequest;
import it.polito.dp2.RNS.sol3.vehClient.jaxb.rnsSystem.PlaceRef;
import it.polito.dp2.RNS.sol3.vehClient.jaxb.rnsSystem.UpdatePositionRequest;
import it.polito.dp2.RNS.sol3.vehClient.jaxb.rnsSystem.UpdateStateRequest;
import it.polito.dp2.RNS.sol3.vehClient.jaxb.rnsSystem.Vehicle;
import it.polito.dp2.RNS.sol3.vehClient.jaxb.rnsSystem.VehicleState;
import it.polito.dp2.RNS.sol3.vehClient.jaxb.rnsSystem.VehicleType;
import it.polito.dp2.RNS.sol3.service.exceptions.ExceptionCodes;

public class VehClientImpl implements VehClient {
	
	private Client client;
	private WebTarget target;
	private Vehicle vehicle;

	public VehClientImpl() {
		client = ClientBuilder.newClient();
		target = client.target(this.getBaseURI());
		vehicle = new Vehicle();
	}
	
	private URI getBaseURI() {
		String baseURI = System.getProperty("it.polito.dp2.RNS.lab3.URL");
		if(baseURI == null) // Property not set, use default value
			baseURI = "http://localhost:8080/RnsSystem/rest";
		
		return UriBuilder.fromUri(baseURI)
				.path("webapi")
				.build();
	}
	
//	private PlaceRef generatePlaceRefFromId(String id) {
//		PlaceRef pr = new PlaceRef();
//		pr.setId(id);
//		return pr;
//	}

	@Override
	public List<String> enter(String plateId, it.polito.dp2.RNS.VehicleType type, String inGate, String destinationId)
			throws ServiceException, UnknownPlaceException, WrongPlaceException, EntranceRefusedException {
		EnterRequest er = new EnterRequest();
		
		er.setId(plateId);
		er.setType(VehicleType.valueOf(type.value()));
		er.setOrigin(inGate);
		er.setDestination(destinationId);

		System.out.println("[Vehicle]\t" + plateId + " Requesting path from " + inGate + " to " + destinationId);
		
		Response response = target.path("vehicles")
				.request()
				.accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(er, MediaType.APPLICATION_JSON));
		
		switch(response.getStatus()) {
			case 201:	// Correct response
				break; 
			case ExceptionCodes.PLACE_NOT_FOUND:	// Unknown place
				throw new UnknownPlaceException("Error in remote operation: "
					+ response.getStatus()
					+ " " + response.getStatusInfo()); 
			case ExceptionCodes.GATE_NOT_FOUND:		// Not a gate
			case ExceptionCodes.INVALID_GATE:		// Gate is not IN/INOUT
				throw new WrongPlaceException("Error in remote operation: "
					+ response.getStatus()
					+ " " + response.getStatusInfo());
			case ExceptionCodes.CONFLICT:
			case ExceptionCodes.PATH_NOT_FOUND:	// Path unavailable
				throw new EntranceRefusedException("Error in remote operation: "
						+ response.getStatus()
						+ " " + response.getStatusInfo());
			default:
				String msg = "Error occurred for vehicle " + plateId + ".\n"
						+ response.getStatus()
						+ " " + response.getStatusInfo();
				System.out.println(msg);
				throw new ServiceException(msg);
		}
		
		vehicle = response.readEntity(Vehicle.class);
		System.out.println("[Vehicle]\tAfter an enter " + vehicle.getId()
				+ " returned with a path from " + vehicle.getOrigin().getId() + " to " + vehicle.getDestination().getId() + "."
				+ " now in position " + vehicle.getPosition().getId() + "."
				+ " State: " + vehicle.getState() + " Type: " + vehicle.getType() + " Time: " + vehicle.getEntryTime().toString());
		return vehicle.getPath().getPlace().stream()
				.map(PlaceRef::getId)
				.peek(p -> System.out.println("[Vehicle]\tI should pass in place " + p))
				.collect(Collectors.toList());
	}

	@Override
	public List<String> move(String newPlace) throws ServiceException, UnknownPlaceException, WrongPlaceException {
		System.out.println("[Vehicle]\t" + vehicle.getId() + " moving to " + newPlace);
		List<String> previousPath = vehicle.getPath().getPlace()
				.stream()
				.map(PlaceRef::getId)
				.collect(Collectors.toList());
		
		UpdatePositionRequest ur = new UpdatePositionRequest();
		ur.setPosition(newPlace);
		
		Response response = target.path("vehicles")
				.path(vehicle.getId())
				.path("position")
				.request()
				.accept(MediaType.APPLICATION_JSON)
				.put(Entity.entity(ur, MediaType.APPLICATION_JSON));
		
		switch(response.getStatus()) {
			case 200:	// Correct response
				break;
			case ExceptionCodes.PLACE_NOT_FOUND:
				throw new UnknownPlaceException("Error in remote operation: "
						+ response.getStatus()
						+ " " + response.getStatusInfo());
			case ExceptionCodes.PATH_NOT_FOUND:	// Path unavailable
				throw new WrongPlaceException("Error in remote operation: "
						+ response.getStatus()
						+ " " + response.getStatusInfo());
			case ExceptionCodes.VEHICLE_NOT_FOUND:
			default:
				String msg = "Error occurred for vehicle " + vehicle.getId() + ".\n"
						+ response.getStatus()
						+ " " + response.getStatusInfo();
				System.out.println(msg);
				throw new ServiceException(msg);
		}
		
		vehicle = response.readEntity(Vehicle.class);
		
		System.out.println("[Vehicle]\tAfter a move " + vehicle.getId()
				+ " returned with a path from " + vehicle.getOrigin().getId() + " to " + vehicle.getDestination().getId() + "."
				+ " now in position " + vehicle.getPosition().getId() + "."
				+ " State: " + vehicle.getState() + " Type: " + vehicle.getType() + " Time: " + vehicle.getEntryTime().toString());
		List<String> newPath = vehicle.getPath().getPlace().stream()
				.map(PlaceRef::getId)
				.collect(Collectors.toList());
		
		if(previousPath.equals(newPath))
			return null;
		else
			return newPath;
	}

	@Override
	public void changeState(it.polito.dp2.RNS.VehicleState newState) throws ServiceException {
		System.out.println("[Vehicle]\t" + vehicle.getId() + " requesting state " + newState);
		UpdateStateRequest ur = new UpdateStateRequest();
		ur.setState(getMyState(newState));
		
		Response response = target.path("vehicles")
				.path(vehicle.getId())
				.path("state")
				.request()
				.accept(MediaType.APPLICATION_JSON)
				.put(Entity.entity(ur, MediaType.APPLICATION_JSON));
		
		if(response.getStatus() != 200) {
			String msg = "Error occurred for vehicle " + vehicle.getId() + ".\n"
					+ response.getStatus()
					+ " " + response.getStatusInfo();
			System.out.println(msg);
			throw new ServiceException(msg);
		}

		vehicle = response.readEntity(Vehicle.class);

		System.out.println("[Vehicle]\tAfter a change state " + vehicle.getId()
				+ " returned with a path from " + vehicle.getOrigin().getId() + " to " + vehicle.getDestination().getId() + "."
				+ " now in position " + vehicle.getPosition().getId() + "."
				+ " State: " + vehicle.getState() + " Type: " + vehicle.getType() + " Time: " + vehicle.getEntryTime().toString());
	}
	
	private VehicleState getMyState(it.polito.dp2.RNS.VehicleState givenState) {
		return VehicleState.valueOf(givenState.value());
	}

	@Override
	public void exit(String outGate) throws ServiceException, UnknownPlaceException, WrongPlaceException {
		System.out.println("[Vehicle]\t" + vehicle.getId() + " requesting exit from gate " + outGate);
		Response response = target.path("vehicles")
				.path(vehicle.getId())
				.queryParam("gate", outGate)
				.request()
				.accept(MediaType.APPLICATION_JSON)
				.delete();
		
		switch(response.getStatus()) {
			case 204:	// Correct response
				break; 
			case ExceptionCodes.PLACE_NOT_FOUND:
				throw new UnknownPlaceException("Error in remote operation: "
						+ response.getStatus()
						+ " " + response.getStatusInfo());
			case ExceptionCodes.GATE_NOT_FOUND:	// Not a gate
			case ExceptionCodes.INVALID_GATE:		// Gate is not IN/INOUT
				throw new WrongPlaceException("Error in remote operation: "
					+ response.getStatus()
					+ " " + response.getStatusInfo());
			default:
				String msg = "Error occurred for vehicle " + vehicle.getId() + ".\n"
						+ response.getStatus()
						+ " " + response.getStatusInfo();
				System.out.println(msg);
				throw new ServiceException(msg);
		}	
	}

}
