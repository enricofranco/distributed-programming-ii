package it.polito.dp2.RNS.sol1.impl;

import java.util.Calendar;

import it.polito.dp2.RNS.PlaceReader;
import it.polito.dp2.RNS.VehicleReader;
import it.polito.dp2.RNS.VehicleState;
import it.polito.dp2.RNS.VehicleType;
import it.polito.dp2.RNS.sol3.admClient.jaxb.rnsSystem.Vehicle;

public class VehicleImpl implements VehicleReader {
	private String id;
	private PlaceReader origin, destination, position;
	private Calendar entryTime;
	private it.polito.dp2.RNS.VehicleState state;
	private it.polito.dp2.RNS.VehicleType type;
	
	public VehicleImpl(Vehicle vehicle, PlaceReader origin, PlaceReader destination, PlaceReader position) {
		this.id = vehicle.getId();
		this.origin = origin;
		this.destination = destination;
		this.entryTime = vehicle.getEntryTime().toGregorianCalendar();
		this.state = VehicleState.valueOf(vehicle.getState().value());
		this.type = VehicleType.valueOf(vehicle.getType().value());
		this.position = position;
	}
	
	@Override
	public String getId() {
		return id;
	}

	@Override
	public PlaceReader getDestination() {
		return destination;
	}

	@Override
	public Calendar getEntryTime() {
		return entryTime;
	}

	@Override
	public PlaceReader getOrigin() {
		return origin;
	}

	@Override
	public PlaceReader getPosition() {
		return position;
	}

	@Override
	public VehicleState getState() {
		return state;
	}

	@Override
	public VehicleType getType() {
		return type;
	}

}
